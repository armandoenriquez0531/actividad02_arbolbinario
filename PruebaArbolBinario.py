'''
Created on 29 oct. 2021

@author: arman
'''
class NodoArbol:
    def __init__(self,dato=-1):
        self.dato=dato
        self.nodoIz=None
        self.nodoDer=None
    
    def getnodoDer(self):
        return self.nodoDer
    def getnodoIz(self):
        return self.nodoIz
    def getDato(self):
        return self.dato
    def setDato(self,dato):
        self.dato=dato
    def setnodoDer(self,nodoDerecho):
        self.nodoDer=nodoDerecho
    def setNodoIz(self,nodoIz):
        self.nodoIz=nodoIz
        
    def igualQue(self,dato):
        return  self.dato==dato
    
    def mayorQue(self,dato):
        return self.dato>dato
    def mayorIgualQue(self,dato):
        return self.dato>=dato
    
    def menorQue(self,dato):
        return self.dato<dato
    def menorIgual(self,dato):
        return self.dato<=dato
    
    
    
class ArbolBinarioBusqueda:
    def __init__(self):
        self.nodoRaiz=None
    #Agregar nodo
    def agregarDato(self,dato):
        nuevoNodo=NodoArbol(dato)
        if (self.nodoRaiz==None):
            self.nodoRaiz=nuevoNodo
        else:
            nodoAux=self.nodoRaiz
            nodoAnterior=None
            while(nodoAux!=None):
                nodoAnterior=nodoAux
                if(dato>nodoAux.getDato()):
                    nodoAux=nodoAux.getnodoDer()
                    if(nodoAux==None):
                        nodoAnterior=nodoAnterior.setnodoDer(nuevoNodo)
                else:
                    nodoAux=nodoAux.getnodoIz()
                    if(nodoAux==None):
                        nodoAnterior=nodoAnterior.setNodoIz(nuevoNodo)
                    
        
    #Eliminar
    def eliminarDato(self,dato):
        self.nodoRaiz = self.eliminar2(self.nodoRaiz, dato)
    def eliminar2(self, padre, dato):

        if padre==None:
            print("No existe el elemento")
        elif padre.mayorQue(dato):
            padre.nodoIz=self.eliminar2(padre.getnodoIz(), dato)
        elif padre.menorQue(dato):
            padre.nodoDer=self.eliminar2(padre.getnodoDer(), dato)
            
        else:

            if padre.getnodoIz()==None:
                padre = padre.getnodoDer()
            elif padre.getnodoDer()==None:
                padre = padre.getnodoIz()
            else:
                self.__eliminarDobleNodo(padre, dato)
            print("Elemento eliminado")

        return padre
    def __eliminarDobleNodo(self, padre, dato):

        anterior = padre
        actual = padre.getnodoIz()

        while(actual.getnodoDer()!=None):
            anterior = actual
            actual = actual.getnodoDer()

        padre.dato = actual.dato

        if padre==anterior:
            anterior.setnodoDer(actual.getnodoIz()) 
        else:
            anterior.setnodoDer(actual.getnodoIz()) 
    
    #Recorridos
    def recorridoPreorden(self,nodoRaiz):
        if(not nodoRaiz==None):
            print(f"{nodoRaiz.getDato()} ->",end=" ")
            self.recorridoPreorden(nodoRaiz.getnodoIz())
            self.recorridoPreorden(nodoRaiz.getnodoDer())
    def recorridoEnorden(self,nodoRaiz):
        if(not nodoRaiz==None):
            self.recorridoPreorden(nodoRaiz.getnodoIz())
            print(f"{nodoRaiz.getDato()} ->",end=" ")
            self.recorridoPreorden(nodoRaiz.getnodoDer())
    def recorridoPostOrden(self,nodoRaiz):
        if(not nodoRaiz==None):
            self.recorridoPreorden(nodoRaiz.getnodoIz())
            self.recorridoPreorden(nodoRaiz.getnodoDer())
            print(f"{nodoRaiz.getDato()} ->",end=" ")
    
    #Busquedas
    def buscarMenor(self):
        if(self.nodoRaiz==None):
            print("No hay elementos")
        else:
            self.obtenerMenor(self.nodoRaiz,self.nodoRaiz.getnodoIz())
    
    def obtenerMenor(self,anterior=NodoArbol,actual=NodoArbol):
        if(actual==None):
            if(anterior.getDato()>-1):
                print(f"El menor es: {anterior.getDato()}")
        else:
            self.obtenerMenor(anterior.getnodoIz(), actual.getnodoIz())
    
    def buscarMayor(self):
        if(self.nodoRaiz==None):
            print("No hay elementos")
        else:
            self.obtenerMayor(self.nodoRaiz, self.nodoRaiz.getnodoDer())
        pass
    def obtenerMayor(self,anterior=NodoArbol,actual=NodoArbol):
        if(actual==None):
            print(f"El mayor es: {anterior.getDato()}")
        else:
            self.obtenerMayor(anterior.getnodoDer(), actual.getnodoDer())
        pass
    
    #-------------------------------------------------------
    #Busqueda
    def buscarDato(self,dato):
        if(self.nodoRaiz==None):
            print("Elemento no encontrado")
        elif(self.nodoRaiz.menorQue(dato)):
            self.buscar(self.nodoRaiz.getnodoDer(), dato)
        elif(self.nodoRaiz.mayorQue(dato)):
            self.buscar(self.nodoRaiz.getnodoDer(), dato)
        else:
            print("Encontrado")
            
    def buscar(self,raiz,dato):
        if(raiz==None):
            print("Elemento no encontrado")
        elif(raiz.menorQue(dato)):
            self.buscar(raiz.getnodoDer(), dato)
        elif(raiz.mayorQue(dato)):
            self.buscar(raiz.getnodoDer(), dato)
        else:
            print("Encontrado")
    
     
#----------------------------
arbol=ArbolBinarioBusqueda()
op=""
arbol.agregarDato(5)
arbol.agregarDato(22)
arbol.agregarDato(7)
arbol.agregarDato(17)
arbol.agregarDato(52)
arbol.agregarDato(1)
while(not op=="7"):
    print("Elige una opcion")
    print("1- Insertar Nodo")
    print("2- Eliminar nodo")
    print("3- Mostrar nodos")
    print("4- Mostrar dato mayor")
    print("5- Mostrar dato menor")
    print("6- Buscar dato")
    print("7- Salir")
    op=input()
    if(op=="1"):
        d=int(input("Ingresa el dato: "))
        arbol.agregarDato(d)
        print("-------------------")
    elif (op=="2"):
        datoE=int(input("Ingresa el dato que deseas eliminar: "))
        arbol.eliminarDato(datoE)
        print("----------------------------------")
    elif (op=="3"):
        print("1- Recorrido en preorden")
        print("2- RecorridoInorden")
        print("3- PostOrden")
        op2=input("Elige una opcion: ")
        if(op2=="1"):
            arbol.recorridoPreorden(arbol.nodoRaiz)
            print()
        elif(op2=="2"):
            arbol.recorridoEnorden(arbol.nodoRaiz)
            print()
        elif(op2=="3"):
            arbol.recorridoPostOrden(arbol.nodoRaiz)
            print()
        else:
            print("No elegiste una opcion correcta")
    elif (op=="4"):
        arbol.buscarMayor()
        print("------------------------------")
    elif (op=="5"):
        arbol.buscarMenor()
        print()
    elif (op=="6"):
        d=int(input("Ingresa el dato que deseas buscar"))
        arbol.buscarDato(d)
        print()
    elif (op=="7"):
        print()